var moment = require('moment');
var date = moment();

var sendData = (from, msg) => {
  return {
    from,
    msg,
    created_at: date.format('hh:ss a')
  };
};

var sendLocation = (from, lat, lng) => {
  return {
    from,
    url: `https://www.google.com/maps?q=${lat},${lng}`,
    created_at: date.format('hh:ss a')
  };
};

module.exports = {
  sendData,
  sendLocation
};

const express = require('express');
const path = require('path');
const http = require('http');
const socketIO = require('socket.io');

var {sendData, sendLocation} = require('./sendData');

const publicPath = path.join(__dirname, './public');
const app = express();
var server = http.createServer(app);
var io = socketIO(server);

app.use(express.static(publicPath));

io.on('connection', (socket) => {
  console.log(' User Connected');

  socket.on('disconnect', () => {
    console.log('User disconnected');
  });

  io.emit('pushData',sendData('Admin', 'New User Joined, Say Welcome.'));

  socket.on('send', (data, callback) => {
    socket.broadcast.emit('pushData', sendData('user', data.msg));
    callback('response data from server');

  });

  socket.on('sendLocation', (coords, callback) => {
    socket.broadcast.emit('pushLocation', sendLocation('Admin', coords.lat, coords.lng));
    callback('response Location from server');
  });
});


server.listen(3000, () => {
  console.log('listenning on port 3000');
});
